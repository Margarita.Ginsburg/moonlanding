package com.example.moonlanding.models

import java.io.Serializable

data class LandingData(
    val height: Int,
    val speed: Int,
    val gravity: Int? = null,
    val fuel: Int
) : Serializable