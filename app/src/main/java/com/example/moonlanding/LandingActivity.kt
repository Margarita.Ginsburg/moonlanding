package com.example.moonlanding

import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.moonlanding.engine.MoonLandingEngine
import com.example.moonlanding.models.LandingData

class LandingActivity: AppCompatActivity() {

    lateinit var landingData: LandingData
    lateinit var tvReportHeight: TextView
    lateinit var tvReportSpeed: TextView
    lateinit var tvReportFuel: TextView
    lateinit var btEnter: Button
    lateinit var etMass: EditText
    lateinit var moonLandingEngine: MoonLandingEngine

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_landing)

        landingData = intent.extras?.get("EXTRA_MESSAGE") as LandingData

        tvReportHeight = findViewById(R.id.tvReportHeight)
        tvReportSpeed = findViewById(R.id.tvReportSpeed)
        tvReportFuel = findViewById(R.id.tvReportFuel)
        btEnter  = findViewById(R.id.btEnter)
        etMass = findViewById(R.id.etMass)

        moonLandingEngine = MoonLandingEngine(landingData)

        tvReportHeight.text = landingData.height.toString() + " meters"
        tvReportFuel.text = landingData.fuel.toString() + " kg"
        tvReportSpeed.text = landingData.speed.toString() + " m/s"

        btEnter.setOnClickListener {

            if (TextUtils.isEmpty(etMass.text)){
                etMass.error = "The filed must not be empty"
                return@setOnClickListener
            }

            val currentData = moonLandingEngine.getLandingParameters(etMass.text.toString().toInt())

            tvReportHeight.text = currentData.height.toString() + " meters"
            tvReportFuel.text = currentData.fuel.toString() + " kg"
            tvReportSpeed.text = currentData.speed.toString() + " m/s"

        }
    }
}