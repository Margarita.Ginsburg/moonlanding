package com.example.moonlanding.engine

import com.example.moonlanding.models.LandingData

class MoonLandingEngine(private val landingData: LandingData) {

    var time = 0
    var speed0 = landingData.speed
    var height0 = landingData.height
    var fuel = landingData.fuel

    open fun getLandingParameters(fuelMass: Int): LandingData {

        val speed = speed0 + landingData.gravity!! * 1 - fuelMass
        val height = height0 - speed * 1

        speed0 = speed
        height0 = height
        time++
        fuel -= fuelMass

        return LandingData(
            height = height,
            speed = speed,
            fuel = fuel
        )
    }
}