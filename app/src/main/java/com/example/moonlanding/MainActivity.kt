package com.example.moonlanding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.moonlanding.models.LandingData

class MainActivity: AppCompatActivity() {

    lateinit var etHeight: EditText
    lateinit var etSpeed: EditText
    lateinit var etGravity: EditText
    lateinit var etFuel: EditText
    lateinit var btStart: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etHeight = findViewById(R.id.etHeight)
        etSpeed = findViewById(R.id.etSpeed)
        etGravity = findViewById(R.id.etGravity)
        etFuel = findViewById(R.id.etFuel)
        btStart = findViewById(R.id.btStart)

        btStart.setOnClickListener {
            val message = "Height = ${etHeight}" +
                    "Speed = ${etSpeed}" +
                    "Fuel = ${etFuel}"
            Log.i("Logcat", message)

            if (TextUtils.isEmpty(etHeight.text)) {
                etHeight.error = "The filed must not be empty"
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(etGravity.text)) {
                etGravity.error = "The filed must not be empty"
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(etSpeed.text)) {
                etSpeed.error = "The filed must not be empty"
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(etFuel.text)) {
                etFuel.error = "The filed must not be empty"
                return@setOnClickListener
            }

            val landingData = LandingData(
                height = etHeight.text.toString().toInt(),
                speed = etSpeed.text.toString().toInt(),
                gravity = etGravity.text.toString().toInt(),
                fuel = etFuel.text.toString().toInt()
            )

            val intent = Intent(this, LandingActivity::class.java)
            intent.putExtra("EXTRA_MESSAGE", landingData)
            startActivity(intent)
        }

    }
}